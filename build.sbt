
name := "sbt-bug-demo"

version := "0.1"

scalaVersion := "2.12.10"

credentials += Credentials(baseDirectory.value / "ivy-credentials")

resolvers += "optrak" at "https://office.optrak.com/code/thirdparty"

val jnlp = "javax.jnlp" % "jnlp" % "1.7"

libraryDependencies ++= Seq(
  jnlp
)
